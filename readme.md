# flatten


## DESCRIPTION

`flatten` - Recursively flatten an array.


**SYNOPSIS:**    
`flatten(arr)`

*PARAMETERS:*    
`arr` Array to flatten.

**RETURN VALUE:**    
New, flattened, array; original array is left unchanged.



## INSTALL

[npm](https://www.npmjs.com/)

```shell
$ npm install @jsbx/flatten
```

[yarn](https://yarnpkg.com)

```shell
$ yarn add @jsbx/flatten
```


# USAGE

```js
var flatten = require('@jsbx/flatten');

flatten([1, [2, [3]], 4, [5]]);
//=> [1,2,3,4,5]
```


